import { PostBlog } from './interfaces/post-blog';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
  private API = "https://jsonplaceholder.typicode.com/posts"

  constructor(private http: HttpClient) { }

  getPostBlog(): Observable<PostBlog>
  {
    return this.http.get<PostBlog>(`${this.API}`);
  }
}