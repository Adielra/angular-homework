export interface PostBlog {
   userId: number;
   id: number;
   title: String;
   body: String;
}
